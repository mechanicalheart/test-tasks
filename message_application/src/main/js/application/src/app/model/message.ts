export class Message {
	id: number;
	senderNumber: string;
	recipientNumber: string;
	sent: boolean;
	text: string;
	sentDate: string;
	numberOfTries: number;
	errorCode: number;
}
