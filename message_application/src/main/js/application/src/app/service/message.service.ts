import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Page } from '../model/page';
import { Observable } from 'rxjs/Observable';
 
@Injectable()
export class MessageService {
 
  private messagesUrl: string;
 
  constructor(private http: HttpClient) {
    this.messagesUrl = 'http://localhost:8080/messages';
  }
 
  public findAll(sender: string, recipient: string, page: string): Observable<Page> {
	const params = new HttpParams().append('sender', sender).append('recipient', recipient).append('page', page);
    return this.http.get<Page>(this.messagesUrl, {params});
  }
 
  public delete(id: number): Observable<{}> {
  const url = `${this.messagesUrl}/${id}`;
  return this.http.delete(url);
  }
}