import { Component, OnInit } from '@angular/core';
import { Message } from './model/message';
import { Page } from './model/page';
import { MessageService } from './service/message.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  messages: Message[];
  sender: string = "";
  recipient: string = "";
  pages: number[];
  currentPage: number = 0;
 
  constructor(private messageService: MessageService) {  }
 
  ngOnInit() {
   	this.getMessages();
  }
  
  getMessages() {
	this.messageService.findAll(this.sender, this.recipient, this.currentPage.toString()).subscribe(data => {
    this.messages = data.content;
	this.pages = [];
	this.currentPage = 0;
	for (let i = 0; i < data.totalPages; i++) {
        this.pages.push(i);
      }
    });
  }
  
  deleteMessage(message) {
	this.messageService.delete(message.id).subscribe(result => {this.getMessages()});
  }
  
  setSelected(p) {
	this.currentPage = p;
	this.getMessages();
  }
  
  resetFilter() {
	this.sender = '';
	this.recipient = '';
	this.getMessages();
  }
}
