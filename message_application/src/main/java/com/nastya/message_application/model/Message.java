package com.nastya.message_application.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String senderNumber;
    private String recipientNumber;
    private boolean sent;
    private String text;
    private LocalDate sentDate;
    private int numberOfTries;
    private int errorCode;

    public Message() {
    }

    public long getId() {
        return id;
    }

    public String getSenderNumber() {
        return senderNumber;
    }

    public void setSenderNumber(String senderNumber) {
        this.senderNumber = senderNumber;
    }

    public String getRecipientNumber() {
        return recipientNumber;
    }

    public void setRecipientNumber(String recipientNumber) {
        this.recipientNumber = recipientNumber;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDate getSentDate() {
        return sentDate;
    }

    public void setSentDate(LocalDate sentDate) {
        this.sentDate = sentDate;
    }

    public int getNumberOfTries() {
        return numberOfTries;
    }

    public void setNumberOfTries(int numberOfTries) {
        this.numberOfTries = numberOfTries;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", senderNumber='" + senderNumber + '\'' +
                ", recipientNumber='" + recipientNumber + '\'' +
                ", sent=" + sent +
                ", text='" + text + '\'' +
                ", sentDate=" + sentDate +
                ", numberOfTries=" + numberOfTries +
                ", errorCode=" + errorCode +
                '}';
    }
}
