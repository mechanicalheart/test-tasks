package com.nastya.message_application.controller;

import com.nastya.message_application.model.Message;
import com.nastya.message_application.repositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class MessageController {

    private final MessageRepository messageRepository;

    @Autowired
    public MessageController(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @GetMapping("/messages")
    @ResponseBody
    public Page<Message> getMessagesWithFilters(@RequestParam(defaultValue = "") String sender,
                                                @RequestParam(defaultValue = "") String recipient,
                                                @RequestParam(value = "page", defaultValue = "0") int page) {
        Page<Message> messages;
        Pageable pageable = PageRequest.of(page, 10);
        if (sender.equals("") && recipient.equals("")) {
            messages = messageRepository.findAll(pageable);
        }
        else if (!sender.equals("") && !recipient.equals("")){
            messages = messageRepository.findBySenderNumberAndRecipientNumber(sender, recipient, pageable);
        }
        else if (!sender.equals("")) {
            messages = messageRepository.findBySenderNumber(sender, pageable);
        }
        else {
            messages = messageRepository.findByRecipientNumber(recipient, pageable);
        }
        return messages;
    }

    @GetMapping("/messages/{id}")
    @ResponseBody
    public Message findMessageById(@PathVariable("id") Message message) {
        return message;
    }

    @RequestMapping(value = "/messages/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteMessage(@PathVariable("id") long id) {

        System.out.println("Deleting message with Id: " + id);

        messageRepository.deleteById(id);
    }

}
