package com.nastya.message_application.repositories;

import com.nastya.message_application.model.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends PagingAndSortingRepository<Message, Long>, QuerydslPredicateExecutor<Message> {

    Page<Message> findBySenderNumber(String senderNumber, Pageable pageable);

    Page<Message> findByRecipientNumber(String recipientNumber, Pageable pageable);

    Page<Message> findBySenderNumberAndRecipientNumber(String senderNumber, String recipientNumber, Pageable pageable);
}
