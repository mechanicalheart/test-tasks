# Web application for messages

This is a small one page web application.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

Things you need to install:

* **Java 1.8.0_241**
* **Apache Maven**
* **Node.js**
* **npm**
* **Angular CLI**

Install Java by typing this in terminal: 
```
sudo add-apt-repository ppa:ts.sch.gr/ppa
sudo apt-get update
sudo apt-get install oracle-java8-installer
```
Check installed version:
```
java -version
javac -version
```

Install Apache Maven:
```
sudo apt-get install mvn
# if that does not work, try
sudo apt-get install maven
```

Install Node.js:
```
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```
npm client command line interface is installed with Node.js by default.

To install the CLI using npm enter the following command:
```
sudo npm install -g @angular/cli
```

### Installing

To install and run this application you need to follow these steps:

* Download repo from my page or clone it by typing this in terminal:

```
git clone https://mechanicalheart@bitbucket.org/mechanicalheart/test-tasks.git
```

* Open the /message_application directory
* Run project from here:

```
mvn spring-boot:run
```

* Then go into /src/main/js/application and use the following command:
```
ng serve --open
```
* Open in browser http://localhost:4200/
* You are done! 

## Built With

* [Intellij IDEA Ultimate](https://www.jetbrains.com/idea/) - The IDE used
* [Apache Maven 3.6.3](https://maven.apache.org/) - Dependency Management
* [Spring](https://spring.io/) - The Java framework used
* [Java 1.8.0_241](https://java.com/ru/download/)
* [Node.js v8.10.0](https://nodejs.org/en/)
* [Angular](https://angular.io/) - The JS framework used

## Authors

[**Anastasiya Lebedeva**](https://bitbucket.org/mechanicalheart)

## Acknowledgments

* Special thanks to my cat for mental support

