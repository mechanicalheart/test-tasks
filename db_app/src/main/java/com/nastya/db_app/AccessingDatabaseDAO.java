package com.nastya.db_app;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;

@Repository
public class AccessingDatabaseDAO {
    private JdbcTemplate jdbcTemplate;

    //public void setDataSource(DataSource dataSource) {
//        jdbcTemplate = new JdbcTemplate(dataSource);
//    }
//
//    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
//        this.jdbcTemplate = jdbcTemplate;
//    }

    public AccessingDatabaseDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

     public int count(String s) {
            String query = s;
            int count = jdbcTemplate.queryForObject(query, Integer.class);
            return count;
        }
}

