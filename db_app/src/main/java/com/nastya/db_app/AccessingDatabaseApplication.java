package com.nastya.db_app;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AccessingDatabaseApplication {

    @Autowired private AccessingDatabaseDAO accessingDatabaseDAO;

    public static void main(String[] args) {
        SpringApplication.run(AccessingDatabaseApplication.class, args);
    }
    @Bean
    CommandLineRunner runner(){
        return args -> {
            int count = accessingDatabaseDAO.count("SELECT COUNT(*) FROM account_phones");
            System.out.println(count);
        };
    }

}