package com.nastya;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ListRand {
    public ListNode Head;
    public ListNode Tail;
    public int Count;

    public void Serialize(FileOutputStream s) {
        List<ListNode> arr = new ArrayList<>();
        ListNode temp = Head;

        do {
            arr.add(temp);
            temp = temp.Next;
        } while (temp != null);

        try {
            for (int i = 0; i < Count; i++) {
                ListNode l = arr.get(i);
                String stringNode = l.Data + ":" + arr.indexOf(l.Rand) + "\n";
                s.write(stringNode.getBytes());
            }
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void Deserialize(FileInputStream s) {
        List<ListNode> arr = new ArrayList<>();
        ListNode temp = new ListNode();
        Count = 0;
        Head = temp;
        String line;

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(s, StandardCharsets.UTF_8));

            while ((line = br.readLine()) != null) {
                Count++;
                temp.Data = line;
                ListNode next = new ListNode();
                temp.Next = next;
                arr.add(temp);
                next.Prev = temp;
                temp = next;
            }

            Tail = temp.Prev;
            Tail.Next = null;

            for (ListNode listNode : arr) {
                String[] split = listNode.Data.split(":");
                listNode.Data = split[0];
                listNode.Rand = arr.get(Integer.parseInt(split[1]));
            }

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
