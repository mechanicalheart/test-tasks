package com.nastya;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class Main {

    static Random rand = new Random();

    public static void main(String[] args) {

        int length = 6;

        ListNode head = new ListNode();

        head.Data = String.valueOf(rand.nextInt(100));

        ListNode tail = head;

        for (int i = 1; i < length; i++) {
            ListNode result = new ListNode();
            result.Prev = tail;
            result.Next = null;
            result.Data = String.valueOf(rand.nextInt(100));
            tail.Next = result;
            tail = result;
        }

        ListNode temp = head;

        for (int i = 0; i < length; i++)
        {
            int k = rand.nextInt(length);
            int j = 0;
            ListNode res = head;
            while (j < k)
            {
                res = res.Next;
                j++;
            }
            temp.Rand = res;
            temp = temp.Next;
        }

        ListRand serializedList = new ListRand();
        serializedList.Head = head;
        serializedList.Tail = tail;
        serializedList.Count = length;

        try (FileOutputStream fos = new FileOutputStream("list.dat", false)) {
            serializedList.Serialize(fos);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ListRand deserializedList = new ListRand();
        try (FileInputStream fis = new FileInputStream("list.dat")) {
            deserializedList.Deserialize(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }

        boolean equal = true;
        ListNode sTemp = serializedList.Head;
        ListNode dTemp = deserializedList.Head;
        for (int i = 0; i < length; i++) {
            if (!(dTemp.Data.equals(sTemp.Data)) || !(dTemp.Rand.Data.equals(sTemp.Rand.Data))) {
                equal = false;
                break;
            }
            sTemp = sTemp.Next;
            dTemp = dTemp.Next;
        }

        if (equal) System.out.println("Success");
        else System.out.println("Fail");

    }

}
