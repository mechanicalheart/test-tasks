# Найти количество клиентов, которые совершали звонок вне сервиса
# в интервале дат 01.01.20-31.01.20
SELECT COUNT(*) FROM calls WHERE accountid_to IS NULL AND (date BETWEEN '2020-01-01' AND '2020-01-31');
# Найти количество звонков, совершённых внутри сервиса в интервале
# дат 15.01.20-20.02.20
SELECT COUNT(*) FROM calls WHERE accountid_from IS NOT NULL AND accountid_to IS NOT NULL AND (date BETWEEN '2020-01-15' AND '2020-02-20');
# Найти количество клиентов на сервисном плане 1002, которым звонили
# извне сервиса в интервале дат 15.04.20-15.05.20
SELECT COUNT(*) FROM accounts JOIN calls ON accounts.id=calls.accountid_to WHERE planid=1002 AND accountid_from IS NULL AND (date BETWEEN '2020-04-15' AND '2020-05-15');
# Найти общее количество звонков, длительностью более минуты,
# совершённых клиентами на сервисном плане 1002 внутри сервиса, на
# регионы с кодами 209 и 415
# Примечание: для упрощения, считаем, что код региона это 2-4 цифры
# в номере, если читать слева направо, например: 1(854)123-4567  -
# 854 это искомый код региона
SELECT COUNT(*) FROM calls
    JOIN accounts a ON a.id=calls.accountid_from
    JOIN account_phones ap on ap.accountid=calls.accountid_to
WHERE accountid_to IS NOT NULL
  AND a.planid=1002
  AND (ap.phonenumber LIKE '_209%' OR ap.phonenumber LIKE '_415%')
  AND duration > 60;
# Найти количество звонков на сервисных планах, стоимостью выше 200$,
# которые были совершены с телефонов любой модели Cisco за всю историю данных.
SELECT COUNT(*) FROM calls
    JOIN accounts a ON a.id=calls.accountid_from
    JOIN plans pl on a.planid=pl.id
    JOIN account_phones ap ON a.id=ap.accountid
    JOIN phones ph ON ap.phoneid=ph.id
WHERE pl.price > 200 AND ph.name LIKE 'Cisco%';