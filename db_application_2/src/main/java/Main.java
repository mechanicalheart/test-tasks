import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Logger;

public class Main {

    private static final Logger log = Logger.getLogger(String.valueOf(Main.class));

    public static DataSource getMySQLDataSource(String username, String password) {
        Properties props = new Properties();
        MysqlDataSource mysqlDS = null;
        try (FileInputStream fis = new FileInputStream("src/main/resources/db.properties")) {
            props.load(fis);
            mysqlDS = new MysqlDataSource();
            mysqlDS.setURL(props.getProperty("DB_URL"));
            mysqlDS.setUser(username);
            mysqlDS.setPassword(password);
        } catch (IOException e) {
            log.info(e.getMessage());
        }
        return mysqlDS;
    }

    public static void main(String[] args) {
        System.out.println("Please enter username: ");
        Scanner scan = new Scanner(System.in);
        String username = scan.nextLine();
        System.out.println("Please enter password: ");
        String password = scan.nextLine();
        DataSource ds = getMySQLDataSource(username, password);

        String query = "";
        try {
            query = new String(Files.readAllBytes(Paths.get("query.txt")));
        } catch (IOException e) {
            log.info(e.getMessage());
        }

        if (ds != null) {
            try (Connection con = ds.getConnection(); Statement stmt = con.createStatement();
                 ResultSet rs = stmt.executeQuery(query)) {
                StringBuilder idsWithInvalidNumber = new StringBuilder();
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String data = rs.getString(2);
                    String substr = data.substring(data.indexOf("from:"));
                    String from = substr.substring(5, substr.indexOf(';'));
                    Phonenumber.PhoneNumber number = phoneUtil.parse(from, "US");
                    if (!phoneUtil.isValidNumber(number)) {
                        idsWithInvalidNumber.append(id).append(", ");
                    }
                }
                idsWithInvalidNumber.delete(idsWithInvalidNumber.length() - 2, idsWithInvalidNumber.length() - 1);
                Writer writer = new OutputStreamWriter(new FileOutputStream("result.txt"), StandardCharsets.UTF_8);
                writer.write(idsWithInvalidNumber.toString());
                writer.close();
            } catch (SQLException | NumberParseException | IOException e) {
                log.info(e.getMessage());
            }
        } else {
            log.info("Datasource is null");
        }
    }

}
